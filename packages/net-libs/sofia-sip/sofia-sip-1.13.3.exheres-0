# Copyright 2012 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user=freeswitch tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="RFC3261 compliant SIP User-Agent library"
LICENCES="LGPL-2.1"

PLATFORMS="~amd64"
SLOT="0"
MYOPTIONS="
    doc
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

RESTRICT="test" # Sydbox violations

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/glib:2
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    test:
        dev-libs/check[>=0.9.4]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-stun
    --enable-nth
    --disable-ntlm
    --disable-memleak-log
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc doxygen' )

src_compile() {
    default

    option doc && emake doxygen
}

src_install() {
    default

    option doc && dodoc -r libsofia-sip-ua{,-glib}/docs/html
}

